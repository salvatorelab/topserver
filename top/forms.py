from django import forms
from django.utils.translation import ugettext_lazy as _
from top.models import Server, Comment
from top.widgets import PictureWidget


class ContactForm(forms.Form):
    email = forms.EmailField(widget=forms.TextInput(), label=_("Email"))
    subject = forms.CharField(widget=forms.TextInput(), label=_("Subject"))
    message = forms.CharField(widget=forms.Textarea(), label=_("Message"))
    captcha = forms.CharField(widget=forms.HiddenInput())


class ServerForm(forms.ModelForm):
    class Meta:
        model = Server
        widgets = {
            'image': PictureWidget(),
        }
        # exclude some fields we don't want the user to modify
        exclude = (
        'notify', 'date', 'owner', 'votes', 'month_votes', 'year_votes', 'online', 'visits', 'conversions', 'premium', 'tags')


class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        # exclude some fields we don't want the user to modify
        exclude = ('server', 'user', 'date')
