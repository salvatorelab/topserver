import logging
from datetime import timedelta

from allauth.account.decorators import verified_email_required
from django.conf import settings
from django.contrib import messages
from django.contrib.postgres.search import SearchVector
from django.core.mail import EmailMultiAlternatives
from django.core.paginator import Paginator
from django.db.models import Avg
from django.http import Http404, HttpResponse, HttpResponseRedirect, JsonResponse
from django.shortcuts import render
from django.template.defaultfilters import slugify
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

from top.forms import CommentForm, ContactForm, ServerForm
from .models import Server, Vote, Comment, Tag, MinecraftStats

logger = logging.getLogger(__name__)


def contact(request):
    email = ""
    asunto = ""
    mensaje = ""
    if request.method == "POST":
        contact_form = ContactForm(request.POST)
        if contact_form.is_valid():
            ip = get_client_ip(request)
            email = contact_form.cleaned_data['email']
            asunto = contact_form.cleaned_data['subject']
            mensaje = contact_form.cleaned_data['message']
            captcha = contact_form.cleaned_data['captcha']
            if isValidCaptcha(captcha):
                to = settings.SITE_EMAIL
                html_content = "Formulario de contacto:<br><br><b>De: </b>%s<br><b>IP:</b> %s<br><b>Asunto:</b>%s<br><b>Mensaje:</b><br>%s" % (
                email, ip, asunto, mensaje)
                msg = EmailMultiAlternatives('Formulario de Contacto', html_content, settings.SITE_EMAIL, [to])
                msg.attach_alternative(html_content, 'text/html')
                try:
                    msg.send()
                    feedback = _("Message sent!")
                    messages.add_message(request, messages.SUCCESS, feedback)
                except Exception:
                    feedback = _("Error sending message :(")
                    messages.add_message(request, messages.ERROR, feedback)
                    logger.error('Error sending mail!')
    else:
        contact_form = ContactForm()

    context = {
        'form': contact_form,
        'email': email,
        'subject': asunto,
        'message': mensaje
    }
    return render(request, 'top/contact.html', context)


def isValidCaptcha(code):
    return True
    secret = settings.RECAPTCHA_PRIVATE_KEY
    url = 'https://www.google.com/recaptcha/api/siteverify'
    data = {'secret': secret, 'response': code}
    response = urllib2.urlopen(url, urllib.urlencode(data))
    result = json.load(response)
    print(result)
    if result['success']:
        if result['score'] > 0.5:
            return True
    return False


def index(request):
    latest_servers_list = Server.objects.recent()[:10]
    voted_servers_list = Server.objects.monthly_votes()[:10]
    premium_servers_list = Server.objects.active_premium().order_by('?')[:10]
    context = {
        'latest_servers_list': latest_servers_list,
        'voted_servers_list': voted_servers_list,
        'premium_servers_list': premium_servers_list,
    }
    return render(request, 'top/index.html', context)


def monthTop(request, pagenum=1):
    pagenum = int(pagenum)  # importante, debe ser int
    all_servers = Server.objects.monthly_votes()
    servers = Paginator(all_servers, 10)
    if pagenum <= servers.num_pages:
        thispage = servers.page(pagenum)
        i = (pagenum - 1) * 10 + 1
        for server in thispage:
            server.rank = i
            i += 1
        context = {
            'servers': thispage,
            'page': pagenum,
            'totalPages': servers.page_range[-1],
            'pagination': pagination(pagenum, servers.page_range)
        }
        return render(request, 'top/month.html', context)
    else:
        raise Http404


def yearTop(request, pagenum=1):
    pagenum = int(pagenum)  # importante, debe ser int
    all_servers = Server.objects.yearly_votes()
    servers = Paginator(all_servers, 10)
    thispage = servers.page(pagenum)
    i = (pagenum - 1) * 10 + 1
    for server in thispage:
        server.rank = i
        i += 1
    context = {
        'servers': thispage,
        'page': pagenum,
        'totalPages': servers.page_range[-1],
        'pagination': pagination(pagenum, servers.page_range)
    }
    return render(request, 'top/year.html', context)


def globalTop(request, pagenum=1):
    pagenum = int(pagenum)  # importante, debe ser int
    all_servers = Server.objects.total_votes()
    servers = Paginator(all_servers, 10)
    thispage = servers.page(pagenum)
    i = (pagenum - 1) * 10 + 1
    for server in thispage:
        server.rank = i
        i += 1
    context = {
        'servers': thispage,
        'page': pagenum,
        'totalPages': servers.page_range[-1],
        'pagination': pagination(pagenum, servers.page_range)
    }
    return render(request, 'top/global.html', context)


def recentTop(request, pagenum=1):
    pagenum = int(pagenum)  # importante, debe ser int
    all_servers = Server.objects.recent()
    servers = Paginator(all_servers, 10)
    thispage = servers.page(pagenum)
    i = (pagenum - 1) * 10 + 1
    for server in thispage:
        server.rank = i
        i += 1
    context = {
        'servers': thispage,
        'page': pagenum,
        'totalPages': servers.page_range[-1],
        'pagination': pagination(pagenum, servers.page_range)
    }
    return render(request, 'top/recent.html', context)


def popularTop(request, pagenum=1):
    pagenum = int(pagenum)  # importante, debe ser int
    servers_by_popularity = Server.objects.popular()
    servers = Paginator(servers_by_popularity, 10)
    thispage = servers.page(pagenum)
    i = (pagenum - 1) * 10 + 1
    for server in thispage:
        server.rank = i
        i += 1
    context = {
        'servers': thispage,
        'page': pagenum,
        'totalPages': servers.page_range[-1],
        'pagination': pagination(pagenum, servers.page_range)
    }
    return render(request, 'top/popular.html', context)


def serverSearch(request):
    query = request.GET.get('q', None)
    if query:
        if settings.DATABASES['default']['ENGINE'] == 'django.db.backends.postgresql':
            servers = Server.objects.filter(name__icontains=query)
            if len(servers) == 0:
                servers = Server.objects.annotate(
                    search = SearchVector('name') + SearchVector('description')
                ).filter(search=query)
        else:
            servers = Server.objects.filter(name__icontains=query)

    context = {
        'query': query,
        'servers': servers if servers else [],
    }
    return render(request, 'top/search.html', context)


def server(request, server_id):
    try:
        server = Server.objects.get(pk=server_id)
        slug = slugify(server.name)
        return HttpResponseRedirect('/top/server/{}/{}/'.format(server.id, slug))
    except Server.DoesNotExist:
        raise Http404


def serverWithSlug(request, server_id, slug=""):
    try:
        server = Server.objects.get(pk=server_id)
        comments = Comment.objects.filter(server=server).order_by('-date')
        avg_events = comments.aggregate(Avg('events'));
        avg_support = comments.aggregate(Avg('support'));
        avg_uptime = comments.aggregate(Avg('uptime'));
    except Server.DoesNotExist:
        raise Http404

    current_slug = slugify(server.name)
    if current_slug != slug:
        return HttpResponseRedirect('/top/server/{}/{}/'.format(server.id, current_slug))

    # update visits
    server.visits += 1
    server.save(update_image=False)

    tags = server.tags.all()
    context = {
        'server': server,
        'tags': tags,
        'comments': comments,
        'avg_events': 0 if avg_events['events__avg'] is None else avg_events['events__avg'],
        'avg_support': 0 if avg_support['support__avg'] is None else avg_support['support__avg'],
        'avg_uptime': 0 if avg_uptime['uptime__avg'] is None else avg_uptime['uptime__avg'],
        'description': server.name,
        'keywords': server.name,
        'form': CommentForm(),
    }
    return render(request, 'top/server.html', context)


def editServer(request, server_id):
    if not request.user.is_authenticated:
        return HttpResponseRedirect('/accounts/login/')

    try:
        server = Server.objects.get(pk=server_id)
    except Server.DoesNotExist:
        raise Http404

    if not server.owner == request.user:
        return HttpResponseRedirect('/top/server/%s/' % (server_id))

    if request.method == "POST":
        form = ServerForm(request.POST, request.FILES, instance=server)
        if form.is_valid():
            server = form.save()
            if ('tags_field' in request.POST):
                if not request.POST['tags_field'] == '':
                    tag_ids = request.POST['tags_field'].split(',')
                    server.tags.clear()
                    if len(tag_ids) > 0:
                        tag_ids = [int(x) for x in tag_ids]
                        for t in tag_ids:
                            # check if tag exists
                            if Tag.objects.filter(id=t).count() > 0:
                                server.tags.add(t)
                    server.save()
                else:
                    server.tags.clear()
                    server.save()

            messages.add_message(request, messages.SUCCESS, _("Server edited"))
            return HttpResponseRedirect('/top/server/{}/'.format(server.id))
        else:
            messages.add_message(request, messages.ERROR, _("[Error] Incorrect data"))
    else:
        form = ServerForm(instance=server)

    server_tags = server.tags.all()
    tags = []
    for tag in Tag.objects.all():
        tags.append({
            'id': tag.id,
            'name': tag.name,
            'active': tag in server_tags
        })

    context = {
        'form': form,
        'server': server,
        'tags': tags
    }

    return render(request, 'top/editServer.html', context)

@verified_email_required
def addServer(request):
    if not request.user.is_authenticated:
        return HttpResponseRedirect('/accounts/login/')
    msg = None
    formulario = None
    if request.method == "POST" and request.user.is_authenticated:
        now = timezone.now()
        server = Server(date=now, votes=0, month_votes=0, year_votes=0, owner=request.user)
        formulario = ServerForm(request.POST, request.FILES, instance=server)
        if formulario.is_valid():
            new_server = formulario.save(commit=False)
            new_server.votes = 0
            new_server.owner = request.user
            new_server.save()
            # redirect the user to edit the new server he has just created
            feedback = _("Server published! You can edit a few details now if you want.")
            messages.add_message(request, messages.SUCCESS, feedback)
            return HttpResponseRedirect('/top/server/%s/edit' % new_server.id)
        else:
            msg = _("[Error] Form is not valid")

    if formulario is None:
        formulario = ServerForm()
    context = {
        'form': formulario,
        'message': msg,
    }
    return render(request, 'top/addServer.html', context)


def voteServer(request, server_id):
    try:
        server = Server.objects.get(pk=server_id)
    except Server.DoesNotExist:
        raise Http404

    if request.user.is_authenticated:
        ip = get_client_ip(request)
        if server.can_be_voted_by(request.user) and server.can_be_voted_by_ip(ip):
            # create a new vote
            vote = Vote()
            vote.server = server
            vote.user = request.user
            vote.date = timezone.now()
            vote.ip = ip
            vote.save()
            # recalculate votes
            server.recalculate_votes()

            # lets notify the server that a user has voted:
            # if server.notify:
            #     # send post request to that URL
            #     post_data = [('event', 'vote'), ('email', request.user.email)]
            #     try:
            #         result = urllib2.urlopen(server.notify, urllib.urlencode(post_data))
            #         content = result.read()
            #         print(content)
            #     except urllib2.URLError as e:
            #         print(e.reason)
        else:
            return HttpResponse(status=400, content=_("[Error] You can only vote once a day for each server"))
    else:
        # user not logged in, redirect to vote
        return HttpResponseRedirect('/accounts/login/')

    return HttpResponse('Ok')


def visitServer(request, server_id):
    try:
        server = Server.objects.get(pk=server_id)
    except Server.DoesNotExist:
        raise Http404

    server.conversions += 1
    server.save(update_image=False)

    return HttpResponseRedirect(server.website)


def commentServer(request, server_id):
    try:
        server = Server.objects.get(pk=server_id)
    except Server.DoesNotExist:
        raise Http404

    if request.user.is_authenticated:
        now = timezone.now()
        comment = Comment(user=request.user, server=server, date=now)
        form = CommentForm(request.POST, instance=comment)
        if form.is_valid():
            # check for spam
            startdate = now - timedelta(hours=1)
            results = Comment.objects.filter(server=server, user=request.user, date__range=[startdate, now]).count()
            if results == 0:
                new_comment = form.save()
                if (new_comment.events > 5) or (new_comment.events < 0):
                    new_comment.events = 5
                if (new_comment.support > 5) or (new_comment.support < 0):
                    new_comment.support = 5
                if (new_comment.uptime > 5) or (new_comment.uptime < 0):
                    new_comment.uptime = 5
                new_comment.save()
            else:
                error = _("[Error] You have to wait 1 hour before posting more comments on this server.")
                messages.add_message(request, messages.ERROR, error)
                return HttpResponseRedirect('/top/server/{}/#comments'.format(server.id))
        else:
            error = _("[Error] Form is not valid")
            messages.add_message(request, messages.ERROR, error)
            return HttpResponseRedirect('/top/server/{}/#comments'.format(server.id))

    return HttpResponseRedirect('/top/server/{}/#comment-{}'.format(server.id, new_comment.id))


def premium(request):
    if request.method == "POST":
        message = _('Premium is under development yet. Thank you for your interest ❤️ we will let you know when it is available!')
        context = {
            "disabledPremiumMessage": message
        }
        return render(request, 'top/premium.html', context)
    return render(request, 'top/premium.html')


def myServers(request):
    if not request.user.is_authenticated:
        return HttpResponseRedirect('/accounts/login/')

    servers = Server.objects.filter(owner=request.user).order_by('-date')
    context = {
        'servers': servers
    }
    return render(request, 'top/myServers.html', context)


def server_stats(request, server_id):
    try:
        server = Server.objects.get(pk=server_id)
        server_stats = MinecraftStats.objects.filter(server=server).order_by('-id')[:300]
        stats_data = []
        for stats in server_stats:
            stats_data.append({
                'date': round(stats.date.timestamp()),
                'players': stats.playersOnline,
                #'maxPlayers': stats.maxPlayers,
                'ping': stats.latency,
            })
    except Server.DoesNotExist:
        raise Http404

    return JsonResponse({'stats': stats_data}, safe=False)


def pagination(current_page, page_range):
    JUMP_PAGES_IF_MORE_THAN = 8
    PAGES_NEXT_TO_CURRENT_PAGE = 3

    pages = []
    if len(page_range) < JUMP_PAGES_IF_MORE_THAN:
        pages = page_range
    else:
        for i in page_range:
            if i == 1:
                pages.append(i)
            elif i == page_range[-1]:
                pages.append(i)
            elif abs(current_page - i) <= PAGES_NEXT_TO_CURRENT_PAGE:
                pages.append(i)
            elif abs(current_page-i) == PAGES_NEXT_TO_CURRENT_PAGE+1:
                pages.append(-1)

    return {
        'current_page': current_page,
        'pages': pages
    }


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[-1].strip()
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip
