from string import Template

from django import forms
from django.conf import settings
from django.utils.safestring import mark_safe


class PictureWidget(forms.widgets.FileInput):
    def render(self, name, value, attrs=None, **kwargs):
        image_preview = ''
        if value and hasattr(value, 'url'):
            image_preview = mark_safe(f'<img src="{value.url}"/><br>')
        image_upload = super().render(name, value, attrs=None, **kwargs)
        return f'{image_preview}{image_upload}'

