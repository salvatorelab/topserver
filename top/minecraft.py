from mcstatus import MinecraftServer
from top.models import MinecraftStats


def stats(serverModel):
    server = MinecraftServer.lookup(serverModel.ip)
    status = server.status(retries=1)
    # print(status.latency)
    # print(status.version.name)
    # print(status.version.protocol)
    # print(status.description)
    # print(status.players.online)
    # print(status.players.max)

    return MinecraftStats(
        server=serverModel,
        playersOnline=status.players.online,
        maxPlayers=status.players.max,
        latency=status.latency,
        version=status.version.name
    )

