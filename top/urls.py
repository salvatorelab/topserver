from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='homeTop'),

    path('month/', views.monthTop, name='monthTop'),
    path('month/page-<int:pagenum>/', views.monthTop, name='monthTop'),

    path('year/', views.yearTop, name='yearTop'),
    path('year/page-<int:pagenum>/', views.yearTop, name='yearTop'),

    path('global/', views.globalTop, name='globalTop'),
    path('global/page-<int:pagenum>/', views.globalTop, name='globalTop'),

    path('recent/', views.recentTop, name='recentTop'),
    path('recent/page-<int:pagenum>/', views.recentTop, name='recentTop'),

    path('popular/', views.popularTop, name='popularTop'),
    path('popular/page-<int:pagenum>/', views.popularTop, name='popularTop'),

    path('premium/', views.premium, name='premium'),

    path('servers/search', views.serverSearch, name='serverSearch'),

    path('servers/', views.myServers, name='myServers'),

    path('server/<int:server_id>/', views.server, name='server'),
    path('server/<int:server_id>/edit', views.editServer, name='editServer'),
    path('server/<int:server_id>/vote', views.voteServer, name='voteServer'),
    path('server/<int:server_id>/visit', views.visitServer, name='visitServer'),
    path('server/<int:server_id>/comment', views.commentServer, name='commentServer'),
    path('server/<int:server_id>/stats', views.server_stats, name='server_stats'),
    path('server/<int:server_id>/<slug:slug>/', views.serverWithSlug, name='serverWithSlug'),
    path('server/add', views.addServer, name='addServer'),
]
