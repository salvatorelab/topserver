"""
A scraper for {{SITE}} that follows page links and creates Server objects automatically
"""
import time
from contextlib import closing
from urllib.parse import urljoin

from bs4 import BeautifulSoup
from requests import get
from requests.exceptions import RequestException

CRAWLING_DELAY_SECONDS = 1
SITE = 'https://minecraftservers.org'

SERVER_TABLE_SELECTOR = 'table.serverlist'
SERVER_NAME_SELECTOR = 'h3'
SERVER_IP_SELECTOR = '.server-ip p'
CURRENT_PAGE_SELECTOR = '.current-page'


def max_pages_reached(page, max_pages):
    if max_pages == 0:
        return False

    return page >= max_pages


def crawl(url=SITE, max_pages=0):
    """
    Crawls the site starting at :url
    :param max_pages: max number of pages to crawl, default is no limit or 0
    :param url: url to start crawling, defaults to SITE
    :return: array of crawled servers
    """
    if max_pages:
        print('Crawling up to', max_pages, 'pages')

    page = 1
    servers = []
    keep_crawling = True
    next_url = url
    while keep_crawling:
        result = crawl_page(next_url)

        for server in result['servers']:
            servers.append(server)

        next_url = result['next']
        keep_crawling = result and next_url and not max_pages_reached(page, max_pages)
        time.sleep(CRAWLING_DELAY_SECONDS)
        page = page+1

    print('Crawling finished')
    return servers


def crawl_page(url):
    """
    Crawls a page with servers and returns those servers and the next URL to crawl
    :param url:
    :return: dict containing 'servers' and 'next' url or None
    """
    content = get_page(url)
    if not content:
        return None

    result = parse_server_list(content)
    return result


def get_page(url):
    """
    Attempts to get the content at `url` by making an HTTP GET request.
    If the content-type of response is some kind of HTML/XML, return the
    text content, otherwise return None.
    """
    print('Crawling', url)
    try:
        with closing(get(url, stream=True)) as resp:
            if is_good_response(resp):
                return resp.content
            else:
                return None

    except RequestException as e:
        log_error('Error during requests to {0} : {1}'.format(url, str(e)))
        return None


def is_good_response(resp):
    """
    Returns True if the response seems to be HTML, False otherwise.
    """
    content_type = resp.headers['Content-Type'].lower()
    return (resp.status_code == 200
            and content_type is not None
            and content_type.find('html') > -1)


def log_error(e):
    """
    It is always a good idea to log errors.
    This function just prints them, but you can
    make it do anything.
    """
    print(e)


def parse_server_list(content):
    servers = []
    html = BeautifulSoup(content, 'html.parser')
    for table in html.select(SERVER_TABLE_SELECTOR):
        for row in table.select('tr'):
            server = parse_server_row(row)
            if server:
                servers.append(server)

    current_page = html.select(CURRENT_PAGE_SELECTOR)
    next_page = None
    for sibling in current_page[0].next_siblings:
        if sibling.name == 'li':
            next_page = sibling.find('a')['href']
            break

    return {
        'servers': servers,
        'next': urljoin(SITE, next_page) if next_page else None,
    }


def parse_server_row(row):
    h3 = row.select(SERVER_NAME_SELECTOR)
    p = row.select(SERVER_IP_SELECTOR)
    if not len(h3):
        return None
    if not len(p):
        return None

    name = h3[0].text.strip()
    ip = p[0].text.strip()
    detail_url = h3[0].find('a')['href']
    details = parse_details(urljoin(SITE, detail_url))

    server = {
        'name': name,
        'ip': ip,
        'website': details['website'] if details else None,
        'description': details['description'] if details else None,
        'image': details['image'] if details else None,
    }
    return server


def parse_details(url):
    content = get_page(url)
    html = BeautifulSoup(content, 'html.parser')

    description = None
    desc_elements = html.select('.desc')
    if len(desc_elements):
        description = desc_elements[0].text

    image = None
    image_elements = html.select('.server-banner')
    if len(image_elements):
        image = urljoin(SITE, image_elements[0]['src'])

    website = None
    for tr in html.select('.server-info tr'):
        tds = tr.select('td')
        if len(tds) == 2:
            if tds[0].text == 'Website':
                website = tds[1].find('a')['href']

    time.sleep(CRAWLING_DELAY_SECONDS)
    return {
        'website': website,
        'description': description,
        'image': image,
    }
