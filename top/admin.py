from django.contrib import admin
from .models import Server, Vote, Comment, Uptime, Tag, ServerAdmin

admin.site.register(Server, ServerAdmin)
admin.site.register(Vote)
admin.site.register(Comment)
admin.site.register(Uptime)
admin.site.register(Tag)
