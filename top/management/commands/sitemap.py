from django.core.management.base import BaseCommand
from django.urls import reverse

from top.models import Server
from django.conf import settings


class Command(BaseCommand):
    help = 'Generate a sitemap.xml file'

    def handle(self, *args, **options):
        sitemap_template = '<?xml version="1.0" encoding="UTF-8"?>\n' +  \
                           '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">\n' \
                           '{}\n' \
                           '</urlset>'
        url_template = '<url><loc>https://' + settings.SITE_DOMAIN + '{}</loc></url>'

        urls = []

        # main urls
        urls.append(url_template.format(reverse('homeTop')))
        urls.append(url_template.format(reverse('monthTop')))
        urls.append(url_template.format(reverse('recentTop')))
        urls.append(url_template.format(reverse('yearTop')))
        urls.append(url_template.format(reverse('globalTop')))
        urls.append(url_template.format(reverse('premium')))

        # server urls
        servers = Server.objects.all()
        for server in servers:
            urls.append(url_template.format(reverse('server', args=[server.id])))

        path = settings.BASE_DIR + "/sitemap.xml"
        sitemap = open(path, "w")
        sitemap.write(sitemap_template.format('\n'.join(urls)))
        sitemap.close()

        self.stdout.write(self.style.SUCCESS('Sitemap successfully saved to %s' % path))
