import os
import shutil
import uuid
from urllib.parse import urlparse

import requests
from django.contrib.auth.models import User
from django.core.files import File
from django.core.management.base import BaseCommand
from django.utils import timezone

from top.models import Server
from top.scrappers.minecraftservers import crawl


class Command(BaseCommand):
    help = 'Look for servers on other websites and get/update them'

    def add_arguments(self, parser):
        parser.add_argument('pages', nargs='?', type=int)

    def handle(self, *args, **options):
        max_pages = 0
        if options['pages']:
            max_pages = options['pages']

        crawled_servers = crawl('https://minecraftservers.org', max_pages)
#        crawled_servers = [
#            {'name': 'Tes/ Server', 'image': 'https://test.com/image.gif', 'ip': '1.1.1.1', 'website': 'http://test.com', 'description': 'test description'}
#        ]
        owner = User.objects.get(id=1)
        for crawled_server in crawled_servers:
            existing_server = Server.objects.filter(ip=crawled_server['ip'])
            if len(existing_server) > 0:
                self.stdout.write(self.style.SUCCESS('Server with IP %s already exists' % crawled_server['ip']))
                continue

            try:
                server = Server(date=timezone.now(), votes=0, month_votes=0, year_votes=0, owner=owner)
                server.name = crawled_server['name']
                server.ip = crawled_server['ip']
                server.website = crawled_server['website']
                server.description = crawled_server['description']
                download = retrieve_image(crawled_server['image'])
                server.image.save(str(uuid.uuid1()) + '.' + download['extension'], File(open(download['filepath'], 'rb')))
                server.save()
                self.stdout.write(self.style.SUCCESS('Server %s created!' % crawled_server['name']))
            except Exception as e:
                self.stdout.write(self.style.ERROR('Error saving server "%s": %s' % (crawled_server, e)))


def retrieve_image(image_url):
    url = urlparse(image_url)
    extension = os.path.basename(url.path).split('.')[-1]

    file_name = 'temporary.' + extension
    absolute_path = os.path.join('/tmp', file_name)

    # save image to temp folder
    r = requests.get(image_url, stream=True)
    if r.status_code == 200:
        with open(absolute_path, 'wb') as f:
            r.raw.decode_content = True
            shutil.copyfileobj(r.raw, f)

    return {
        'filepath': absolute_path,
        'extension': extension
    }
