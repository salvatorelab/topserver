from django.core.management.base import BaseCommand

from top.models import Server


class Command(BaseCommand):
    help = 'Recalculate vote counters for one or all servers (year, month and total)'

    def add_arguments(self, parser):
        parser.add_argument('server_id', nargs='?', type=int)

    def handle(self, *args, **options):
        if options['server_id']:
            server = Server.objects.get(pk=options['server_id'])
            server.recalculate_votes()
            self.stdout.write(self.style.SUCCESS('Server votes recalculated for server_id=%s' % server.id))
        else:
            servers = Server.objects.all()
            for server in servers:
                server.recalculate_votes()
                self.stdout.write(self.style.SUCCESS('Server votes recalculated for server_id=%s' % server.id))
