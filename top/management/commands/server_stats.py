from concurrent.futures.thread import ThreadPoolExecutor

from django.core.management.base import BaseCommand, CommandError

from top.minecraft import stats
from top.models import Server
from top.models import ServerPopularity


class Command(BaseCommand):
    help = 'Updates server stats'

    def add_arguments(self, parser):
        parser.add_argument('server_id', nargs='?', type=int)

    def handle(self, *args, **options):
        if options['server_id']:
            self.server_id_stats(options['server_id'])
            return

        servers = Server.objects.all()

        with ThreadPoolExecutor(max_workers=48) as executor:
            for server in servers:
                future = executor.submit(self.server_stats, server)
                #future.result()

    def server_id_stats(self, server_id):
        try:
            server = Server.objects.get(pk=server_id)
        except Server.DoesNotExist:
            raise CommandError('Server "%s" does not exist' % server_id)
        self.server_stats(server)

    def server_stats(self, server):
        if server.ip:
            try:
                server_stats = stats(server)
                print(server_stats)
                server_stats.save()

                # update popularity ranking
                # popularity = players online
                try:
                    server_popularity = ServerPopularity.objects.get(server=server)
                    server_popularity.popularity = server_stats.playersOnline
                    server_popularity.save()
                except ServerPopularity.DoesNotExist:
                    server_popularity = ServerPopularity(
                        server=server,
                        popularity=server_stats.playersOnline
                    )
                    server_popularity.save()

            except Exception as e:
                self.stdout.write(self.style.ERROR('Error retrieving stats for server "%s": %s' % (server.id, e)))

        self.stdout.write(self.style.SUCCESS('Server stats registered for ID "%s"' % server.id))