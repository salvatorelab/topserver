from datetime import timedelta
from django.utils import timezone

from django.core.management.base import BaseCommand

from top.models import MinecraftStats


class Command(BaseCommand):
    help = 'Delete old server stats to keep the database healthy'

    def handle(self, *args, **options):
        days = 3
        MinecraftStats.objects.filter(date__lte=timezone.now() - timedelta(days=days)).delete()

        count = MinecraftStats.objects.all().count()
        self.stdout.write(self.style.SUCCESS('Server stats older than %s days deleted. Number of database entries now: %s' % (days, count)))
