from django.core.management.base import BaseCommand, CommandError

from top.models import Server, MinecraftStats, ServerPopularity


class Command(BaseCommand):
    help = 'Check servers are online'

    def add_arguments(self, parser):
        parser.add_argument('server_id', nargs='?', type=int)

    def handle(self, *args, **options):
        if options['server_id']:
            try:
                server = Server.objects.get(pk=options['server_id'])
                self.update_online_status(server)
            except Server.DoesNotExist:
                raise CommandError('Server "%s" does not exist' % options['server_id'])
            return

        servers = Server.objects.all()
        for server in servers:
            self.update_online_status(server)

        self.stdout.write(self.style.SUCCESS('Server online status updated'))

    def update_online_status(self, server):
        stat_count = MinecraftStats.objects.filter(server=server).count()
        if stat_count is None:
            stat_count = 0

        # server has no stats but it says it is online -> wrong
        if stat_count == 0 and server.online is True:
            server.online = False
            server.save()
            self.stdout.write(self.style.ERROR('Server %s is now OFFLINE' % server.id))
            # delete entry on ServerPopularity
            try:
                server_popularity = ServerPopularity.objects.get(server=server)
                server_popularity.delete()
            except ServerPopularity.DoesNotExist:
                pass

        # server has stats but says it is offline -> wrong
        elif stat_count > 0 and server.online is False:
            server.online = True
            server.save()
            self.stdout.write(self.style.SUCCESS('Server %s is now ONLINE' % server.id))

