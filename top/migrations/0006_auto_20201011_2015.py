# Generated by Django 3.0.6 on 2020-10-11 18:15

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('top', '0005_minecraftstats'),
    ]

    operations = [
        migrations.AlterField(
            model_name='comment',
            name='server',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='top.Server'),
        ),
        migrations.AlterField(
            model_name='comment',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='minecraftstats',
            name='server',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='top.Server'),
        ),
        migrations.AlterField(
            model_name='server',
            name='ip',
            field=models.CharField(help_text='This can also be a domain or subdomain.', max_length=200, verbose_name="Server's IP/Domain"),
        ),
        migrations.AlterField(
            model_name='uptime',
            name='server',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='top.Server'),
        ),
        migrations.AlterField(
            model_name='vote',
            name='server',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='top.Server'),
        ),
        migrations.AlterField(
            model_name='vote',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
        migrations.CreateModel(
            name='ServerPopularity',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('popularity', models.IntegerField(default=0)),
                ('server', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='top.Server')),
            ],
        ),
    ]
