import datetime

from PIL import Image as img
from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

from django.contrib import admin


class ServerManager(models.Manager):
    def monthly_votes(self):
        return Server.objects.order_by('-month_votes')

    def yearly_votes(self):
        return Server.objects.order_by('-year_votes')

    def total_votes(self):
        return Server.objects.order_by('-votes')

    def recent(self):
        return Server.objects.order_by('-date')

    def popular(self):
        return ServerPopularity.objects.order_by('-popularity')

    def active_premium(self):
        now = timezone.now()
        servers = Server.objects.filter(premium__gte=now)
        return servers


class Server(models.Model):
    name = models.CharField(max_length=200, verbose_name=_("Server's name"))
    website = models.URLField(max_length=200, help_text=_("If you don't have a website you can link to your Discord for example"))
    ip = models.CharField(max_length=200, verbose_name=_("Server's IP/Domain"), help_text=_("This can also be a domain or subdomain."))
    description = models.TextField(max_length=350, verbose_name=_("Description"))
    image = models.ImageField(upload_to='top/servers', verbose_name=_("Image"),
                              help_text=_('Maximum dimensions: %(dim)s px') % {'dim': '500x150'})
    owner = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    date = models.DateTimeField()
    online = models.BooleanField(default=True)
    votes = models.IntegerField(default=0)
    month_votes = models.IntegerField(default=0)
    year_votes = models.IntegerField(default=0)
    visits = models.BigIntegerField(default=0)
    conversions = models.BigIntegerField(default=0)
    premium = models.DateTimeField(blank=True, null=True)
    tags = models.ManyToManyField('Tag', blank=True)
    notify = models.URLField(max_length=200, blank=True, null=True, verbose_name=_("Vote4Coins URL script"),
                             help_text=_("<a href=\"/vote4coins\" target=\"_blank\">More info here</a>"))
    objects = ServerManager()

    def can_be_voted_by(self, u):
        # if the user has already voted this server, return false. else, true
        now = timezone.now()
        allowed = now - datetime.timedelta(days=1)  # only one vote per day and user!
        votes = Vote.objects.filter(server=self, user=u, date__gte=allowed)
        if Vote.objects.filter(server=self, user=u, date__gte=allowed):
            return False
        else:
            return True

    def can_be_voted_by_ip(self, ipaddress):
        # if the IP has already voted this server, return false. else, true
        now = timezone.now()
        allowed = now - datetime.timedelta(days=1)  # only one vote per day and user!
        if Vote.objects.filter(server=self, ip=ipaddress, date__gte=allowed):
            return False
        else:
            return True

    def recalculate_votes(self):
        now = timezone.now()
        month_ago = now - datetime.timedelta(days=30)
        year_ago = now - datetime.timedelta(days=365)
        self.votes = Vote.objects.filter(server=self).count()
        self.month_votes = Vote.objects.filter(date__range=[month_ago, now], server=self).count()
        self.year_votes = Vote.objects.filter(date__range=[year_ago, now], server=self).count()
        self.save(update_image=False)

    def __str__(self):
        return u"%s" % (self.name)

    # override save method so that we can resize the uploaded image if needed
    def save(self, update_image=True, *args, **kwargs):
        super(Server, self).save(*args, **kwargs)
        if update_image:
            self.image_resize()

    def image_resize(self):
        pw = self.image.width
        ph = self.image.height
        mw = 500  # max width
        mh = 150  # max height

        if (pw > mw) or (ph > mh):
            # we require a resize
            # load the image
            filename = str(self.image.path)
            imageObj = img.open(filename)
            maxRatio = 1

            ratioWidth = float(pw) / mw;
            ratioHeight = float(ph) / mh;
            if (ratioWidth > ratioHeight):
                maxRatio = ratioWidth
            else:
                maxRatio = ratioHeight

            pw = int(pw / maxRatio);
            ph = int(ph / maxRatio);

            imageObj = imageObj.resize((pw, ph), img.ANTIALIAS)
            imageObj.save(filename)

    @property
    def is_premium(self):
        now = timezone.now()
        if self.premium is not None and self.premium > now:
            return True
        return False


class Uptime(models.Model):
    server = models.ForeignKey(Server, on_delete=models.CASCADE)
    day = models.DateTimeField()
    ok = models.IntegerField(default=0)
    nok = models.IntegerField(default=0)


class Vote(models.Model):
    server = models.ForeignKey(Server, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    date = models.DateTimeField(auto_now_add=True)
    ip = models.CharField(max_length=200, blank=True)

    def __str__(self):
        return u"%s voted %s" % (self.user, self.server)


class Comment(models.Model):
    server = models.ForeignKey(Server, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    date = models.DateTimeField(auto_now_add=True)
    comment = models.TextField(max_length=350)
    events = models.FloatField()
    support = models.FloatField()
    uptime = models.FloatField()

    def __str__(self):
        return u"%s commented on %s" % (self.user, self.server)


class Tag(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return u"%s" % (self.name)


class MinecraftStats(models.Model):
    server = models.ForeignKey(Server, on_delete=models.CASCADE)
    date = models.DateTimeField(auto_now_add=True)
    playersOnline = models.IntegerField(default=0)
    maxPlayers = models.IntegerField(default=0)
    latency = models.IntegerField(default=0)
    version = models.TextField(max_length=350)

    def __str__(self):
        return u"%s/%s players, %sms ping, v=%s" % (self.playersOnline, self.maxPlayers, self.latency, self.version)


class ServerPopularity(models.Model):
    server = models.ForeignKey(Server, on_delete=models.CASCADE)
    popularity = models.IntegerField(default=0)

    def __str__(self):
        return u"Server %s (id=%s) popularity=%s" % (self.server.name, self.server.id, self.popularity)


class ServerAdmin(admin.ModelAdmin):
    ordering = ['date']
    search_fields = ['name', 'ip']

