from django.conf import settings


def inject_settings(request):
    return {
        'SITE_NAME': settings.SITE_NAME,
        'SITE_DOMAIN': settings.SITE_DOMAIN,
        'SITE_EMAIL': settings.SITE_EMAIL,
        'GAME_NAME': settings.GAME_NAME,
        'RECAPTCHA_PUBLIC_KEY': settings.RECAPTCHA_PUBLIC_KEY,
        'GOOGLE_ANALYTICS_ID': settings.GOOGLE_ANALYTICS_ID,
    }


def inject_language(request):
    return {'LANG': request.LANGUAGE_CODE}

