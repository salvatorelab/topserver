# TopServer
This is the software that runs topminecraftserver.net but you can use it for other games and adapt it to your needs.  
Just modify the `settings.py` file and change the SITE_NAME, SITE_DOMAIN, SITE_EMAIL and GAME_NAME variables.  

### Installation
Clone the repo and run `pip3 install requirements.txt -r`.  
You will need Python 3 and pip for that to work. 
Then, run `python3 manage.py migrate` to create the database schema, by default sqlite.  
Create an admin user for the site with `python3 manage.py createsuperuser`.
That's it, run the server with `python3 manage.py runserver` like you would do with any Django project.

### Configure
Go to the `/admin` panel and make sure that under **Sites** you have edited the default *example.com* site. Just replace it with your domain name.
#### Discord login
To enable Discord you will have to create an App with them: https://discord.com/developers/applications  
The only important thing there is the OAuth2 callback url, change it to: http://youtdomain.com/accounts/discord/login/callback/ 
Once you have the key and secret, go to the `/admin` panel and add a **Social application**. Paste your client id and secret there. Don't worry about the key field, you don't need that for Discord. Add the domain and you are done.
#### Other login integrations
Go to `settings.py` and under **INSTALLED_APPS** uncomment the lines that start with `allauth.` that you want to enable. You will then be able to follow similar steps than with Discord. For more information please go to allauth docs: https://django-allauth.readthedocs.io/en/latest/providers.html
#### Cron tasks
These are the recommended tasks or commands we have available:
 
**Retrieve Minecraft statistics**: This task can run every few minutes, here is an example you can add to crontab: 
 - `*/5 * * * * /usr/bin/python3 /home/youruser/topserver/manage.py server_stats --settings=topserver.settings-production`

**Delete old Minecraft statistics**: Run this once a day or at least once a week, here is an example you can add to crontab: 
 - `0 0 * * * /usr/bin/python3 /home/youruser/topserver/manage.py server_stats_cleanup --settings=topserver.settings-production`

**Update Minecraft server status**: Based on Minecraft statistics set the online status to True or False. Run this once a day for example: 
 - `0 8 * * * /usr/bin/python3 /home/youruser/topserver/manage.py server_stats --settings=topserver.settings-production`  
 *Make sure that Minecraft statistics and the cleanup job are working, because this job depends on the other two ones to work properly.*

### Production
#### Settings file
Create a `settings-production.py` file next to `settings.py`.  
That is the file that WSGI will use.  
Make sure to disable `DEBUG` and use a different secret.

#### manage.py
If you are using `manage.py` in production, pass the settings file, for example:

    python3 manage.py shell --settings=topserver.settings-production
  
You will probably find it useful when creating the database schema the first time:

    python3 manage.py migrate --settings=topserver.settings-production

#### Optimizations
If you find that server stats are taking too long to show up, make sure that:
1. the `server_stats_cleanup` command is being executed regularly (once a day would be perfect)
2. for PostgreSQL you can manually run `VACUUM "top_minecraftstats"` to make queries faster after you have deleted many rows